# PiVPN and Pi-hole on a DigitalOcean VPS

Work-in-progress notes for setting up a VPS on [DigitalOcean](https://www.digitalocean.com/) with
- [PiVPN](https://pivpn.io/),
- [Pi-hole](https://pi-hole.net/),
- split-tunneling,
- and [Unbound](https://nlnetlabs.nl/projects/unbound/about/) for [DNS over TLS](https://teddit.net/r/privacytoolsIO/comments/llqd7h/quad9_move_to_switzerland/gntbmz9/) forwarding.

Inspired by https://github.com/mgrimace/PiHole-with-PiVPN-and-Unbound-on-VPS-.

---

## [Create an Ubuntu VPS on DigitalOcean](https://www.digitalocean.com/docs/droplets/how-to/create/).

## [Create new DigitalOcean firewall and assign to VPS](https://www.digitalocean.com/docs/networking/firewalls/how-to/create/).

- Inbound Rules

    - `SSH, TCP, 22, <home IP address>`
    - `Custom, UDP, <desired WireGuard port>, All IPv4, All IPv6`

## [Create a new sudo user on Ubuntu and allow SSH login](https://thucnc.medium.com/how-to-create-a-sudo-user-on-ubuntu-and-allow-ssh-login-20e28065d9ff).

## [Disable logging in as root](https://askubuntu.com/a/27561).

Add the following to `/etc/ssh/sshd_config`.

```
PermitRootLogin no
DenyUsers root
```

Restart SSH service.
```
sudo service ssh restart
```

## [Create DigitalOcean snapshot to save your progress](https://www.digitalocean.com/docs/images/snapshots/how-to/snapshot-droplets/).

## Disable `systemd-resolved`.
```
sudo systemctl stop systemd-resolved
sudo systemctl disable systemd-resolved
```

Delete the symlink `/etc/resolv.conf`.
```
sudo rm /etc/resolv.conf
```

Recreate `/etc/resolv.conf`.
```
sudo vim /etc/resolv.conf
```

Add a nameserver which the VPS will use for resolving network requests (such as updating the Pi-hole blocklists or VPS packages).
```
nameserver 9.9.9.9 # Quad9
nameserver 149.112.112.112 # Quad9
```

Validate nameservers.
```
dig resolver.dnscrypt.info TXT
```

[Optional] Validate [QNAME minimization](https://www.isc.org/blogs/qname-minimization-and-privacy/)
```
dig txt qnamemintest.internet.nl +short
```

Add the hostname (found in `/etc/hostname`) to `/etc/hosts` (e.g. `sudo vim /etc/hosts`):
```
127.0.0.1 localhost [hostname]
```

## [Create DigitalOcean snapshot to save your progress](https://www.digitalocean.com/docs/images/snapshots/how-to/snapshot-droplets/).

## Install Pi-hole
Create an empty file, `dhcpcd.conf` under `/etc` so [Pi-hole enables configuration with a static IP during setup](https://github.com/pi-hole/pi-hole/blob/b5e0f142ccefe9b1b59b94d7e82d3052efbfdcd0/automated%20install/basic-install.sh#L761).

Inspect with [curlbash.io](https://curlbash.io/inspect?url=https://install.pi-hole.net).

```
curl -sSL https://install.pi-hole.net | bash
```
## Install PiVPN
Inspect with [curlbash.io](https://curlbash.io/inspect?url=https://install.pivpn.io).

```
curl -L https://install.pivpn.io | bash
```

## [Optional] Setup Split-Tunneling in WireGuard client configs.

```
[Interface]
PrivateKey = <redacted>
Address = 10.6.0.3/24
DNS = 10.6.0.1

[Peer]
PublicKey = <redacted>
PresharedKey = <redacted>
Endpoint = <Pi-hole IP address / VPS IPv4 address>:<WireGuard port>
AllowedIPs = 10.6.0.1/32
PersistentKeepalive = 25
```

## [Optional] [Enable IPv6 on droplet/WireGuard/client configs](https://github.com/pivpn/pivpn/issues/259#issuecomment-699619034)

Uncomment `net.ipv6.conf.all.forwarding` in `/etc/sysctl.conf`. 

Reload the config.
```
sysctl --system
```

Reload WireGuard.
```
wg-quick down wg0; wg-quick up wg0
```

Restart client's WireGuard connection.

## [Optional] Setup DNS over TLS forwarding with Unbound.

```
apt install unbound
```

Setup DNS over TLS (DoT) forwarding to Quad9. More DoT providers can be found at https://encrypted-dns.party/. Use the below file when following [Pi-hole's guide](https://docs.pi-hole.net/guides/dns/unbound/).

`vim /etc/unbound/unbound.conf.d/pi-hole.conf`

```
server:
port: 5335
tls-cert-bundle: /etc/ssl/certs/ca-certificates.crt

forward-zone:
name: "."
forward-tls-upstream: yes
forward-addr: 9.9.9.9@853#dns.quad9.net
forward-addr: 2620:fe::fe@853#dns.quad9.net
forward-addr: 149.112.112.112@853#dns.quad9.net
forward-addr: 2620:fe::9@853#dns.quad9.net
```

## [Optional] Add additional filter lists
- [Personal collection](https://gitlab.com/-/snippets/2083166)
- https://firebog.net/
- https://github.com/nextdns/metadata
- https://filterlists.com/
- https://rethinkdns.com/configure

## [Optional] Configure Pi-hole to update blocklists more frequently.
- https://web.archive.org/web/20211212233027/https://www.bentasker.co.uk/posts/blog/privacy/467-configuring-pi-hole-to-update-blocklists-more-regularly.html

## Troubleshooting

- Reload WireGuard
```
wg-quick down wg0; wg-quick up wg0
```

- Listen on all interfaces, permitting origins from one hop away (LAN). This is the default behavior, which also helps prevents open resolvers ([ref](https://discourse.pi-hole.net/t/the-pihole-command-with-examples/738)).
```
pihole -a -i local
```

- Check Pi-hole's status
```
pihole status
```

- Check PiVPN's status
```
pivpn debug
```

- Check WireGuard service status
```
systemctl status wg-quick@wg0.service
```

- Restart WireGuard service
```
sudo systemctl restart wg-quick@wg0
```